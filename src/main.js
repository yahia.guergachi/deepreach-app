import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import HomeVue from '../src/components/Home.vue'
import LoginVue from '../src/components/Login.vue'

Vue.config.productionTip = false

Vue.use(VueRouter);

const routes = [
  { path: '/', name:'Login',component: LoginVue },
  { path: '/home', name:'Home',component: HomeVue },
]

// 3. Create the router instance and pass the `routes` option
// You can pass in additional options here, but let's
// keep it simple for now.
const router = new VueRouter({
  routes :routes// short for `routes: routes`
})

new Vue({
  render: h => h(App),
  router

}).$mount('#app')
