# app


## STEPS TO FOLLOW :
1- run the command npm install to install all the dependencies
2- launch the app with npm run serve and connect to localhost:8080
3- You can sign in with 'admin' as login and 'hello' as password
4- You can navigate and try the different features
    a- Use the filter and sort the table in the first tab
    b- Generate new charts with the button randomize in the secund tab
    c- edit the budget for the different campains in the third tab
    d- enable/disable some parameters in the fourth tab

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
